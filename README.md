# Documentation

## Install & run

```
cp env.example .env.local
```

By default `env.example` is fill with office position informations and Galileo Satellite category (you can find [here](https://www.n2yo.com/api/#above) )

__add [n2yo.com](https://www.n2yo.com/api/) API key in your `.env.local`__

```
docker-compose up
```

then just go to [http://localhost:3000](http://localhost:3000)

It should works... Or I screwer up :D

## Explanations

In addition to styled-components, React and NextJS.
| I'm used                   | why?                                                                                       |
|----------------------------|--------------------------------------------------------------------------------------------|
| Apollo (Client and Server) | I really like that stuff and I'm used to used it. But clearly RestFull API works fine too  |
| axios                      | For request the n2yo API (I also could used node-fetch)                                    |
| tailwind + twin.macro      | Clearly not justified. I just want to test that way to used tailwind and styled-components |

### Point 2: Keep a local copy
Like use a storage or a database wasn't a :point_up:.\
I simply used an Http cache-control policy, with 2 minutes value for caching API result.\
So n2yo API could be only called one time every two minutes.\
With Galileo sattelites and all the calls I needs It should be fine.\
Bad effect of that, new data (or button refresh click) could be reached only after two minutes.

### Point 3: Orbital speed
I found for circular orbit, velocity seems to equal: √(GM/ satalt) (thanks wikipedia)\
So I tried to simplified this equation to a more usable value.\
And If I remember well my maths lessons. The biggest satellite altitude will be the lower equation result will be.\
So with this thinking. Sorting by orbital velocity should give the same result than sort ascending by satellite altitude.\

### Questions

> 1. How would you modify your project to handle horizontal scaling in such way we do not keep multiple copies of satellites data?

We could use a memcached or redis for store cache information should be better than just cache-control

> 2. Imagine we do not track a dozen of satellites, but a million. How would you change your project to handle it?

First things to do try to have a premium API_KEY or a better API. Because 1 million with 1000 requests/hour... It's a bit short.

And then
 - Not let user ask for information. Just automatically get and keep all informations We could from API (with cron for example)
 - When database is filled, find what we can do ourself with those data without the API.
 - Provides front information from our database not from the API

> 3. What metrics should be produced by your software? What would they be useful for? Propose an implementation using Prometheus protocol.

I never used Prometheus protocol. So I'm sorry I can't help for this question.\
But we could store metrics about:
- nb n2yo request: Just to see how we use API
- nb client request: to see how engineers use our client
- latitude / longitude of engineers who used the application: to understand where they got lost ;)

> 4. Which assertion in the requirements and constraints is a scientific mistake, if any? This question won't give you any point.

Give to a person lost the position of satellites above our head... It's look like a Perceval (Kaamelott) idea.


# Statement

## Aye Engineer !

You've been selected after a long training period to help POSOS develop its space program. We've watched all Space X launches, played KSP a lot and we've even built a LEGO Saturn V rocket. We're now ready to fly, trust me.

![POSOS experimental crew in orbit](statics/ksp.jpg)

### Your job

Beforehand, we need to develop some software to help us get our feet off the ground. The most important tool we need right now will help our lead engineer to find its way back to our office. He got lost when he went out buying some coffee and never came back. 

We're not sure if it was voluntary though, so let's try sending him a hint about our **offices geographic position** (which is `Latitude 48.8731039° North` and `Longitude 2.3128007° East`, about 87 meters above sea level).

### How to do that

Our scientists team has determined the most efficient way to provide our lead engineer with the most useful information to find his way back. We'll send him the current position of Galileo satellites in the sky above our office, so he can compute himself everything he needs by finding them over his head. We're not quite sure if it's what he needs but we're sure this information will help him. 

Hopefully, an API already exists that we'll do most of the job for us, you can find it there: [n2yo.com](https://www.n2yo.com/api/). Read the doc, you should find something that works. If you need an API key we can give one, or you can register and get one. Anyway, this shouldn't be hardcoded of course.

This API is nice but is not really usable as-is. So you'll need to build a lite interface:

1. First, your app should provide a nice button in the browser that writes back the satellites positions. If you're able to draw a nice looking-result, it would be nicer, but we don't expect you to have such high-grade skills. We don't want the whole page to reload each time you push the button, it's ugly and so '90-ish.

2. Second, our requests count per hour is limited. _Imagine all the people_, err... if all the employees got lost at the same time, if they'd all simutanously requested the service we'd be soon kicked off the API. We should keep a local copy of satellites data to be sure we don't kill the n2yo.com API.

3. Because the fastest satellite will be the first out of sight, please sort them by [orbital velocity](https://en.wikipedia.org/wiki/Orbital_speed) (assuming their orbit is circular around the Earth).

4. If you can also predict the visual passes of the satellites above our position for the next 2 days, it would greatly help our lost lead engineer.

### Output

#### Software code

The first and main deliverable is your code. Please implement a software that fullfills our need, and respects the following constraints.

**Tech stack**
(:point_up: == mandatory, :woman_shrugging: == optional)

- :point_up: React. _If you want to use a framework_, the only one allowed would be [NextJS](https://nextjs.org/docs/)
- :point_up: css/scsss modules or styled components
- :point_up: Docker
- :woman_shrugging: Typescript backend

**Guidelines**
- it should be runnable as one or many Docker container. Provide any needed Docker and Docker Compose file you may find necessary. If your work is not Docker-compatible please justify and provide very detailed running documentation. A software that is not runnable by our 5 years old engineers will be eliminated.
- it should respect best security and software engineering practices

You should send us the link to a public `git` repository hosting your code.
