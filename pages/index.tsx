import type { NextPage } from 'next'
import Head from 'next/head'
import Image from 'next/image'
import styled from 'styled-components';
import tw from 'twin.macro';
import { useQuery, gql } from '@apollo/client';

import ListAbove from '@/components/list-above/ListAbove';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: auto;
  ${tw`w-full lg:w-3/5`}
`;

const List = styled(ListAbove)`
  ${tw`w-full`}
`;

const query = gql`
  query above {
    above {
      category
      satellites {
        satid
        satname
        satlat
        satlng
        satalt
        passes
      }
    }
  }
`;


const Home: NextPage = () => {
  const { loading, data, refetch, error } = useQuery(query);

  if (error) {
    return (
      <Container>
        Iups something bad happened
      </Container>
    );
  }

  if (loading) {
    return (
      <Container>loading ... </Container>
    );
  }

  return (
    <Container>
      <List
        {...data?.above}
        onRefresh={() => refetch()}
      />
    </Container>
  );
};

export default Home;
