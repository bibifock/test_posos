import type { AppProps } from 'next/app'
import { ApolloProvider } from '@apollo/client';

import GlobalStyles from '@/components/styles/GlobalStyles';
import client from '@/core/graphql/client';

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <ApolloProvider client={client}>
      <GlobalStyles />
      <Component {...pageProps} />
    </ApolloProvider>
  );
}

export default MyApp
