import type { NextApiRequest, NextApiResponse } from 'next'
import { gql, ApolloServer } from 'apollo-server-micro';
import { ApolloServerPluginLandingPageGraphQLPlayground, ApolloServerPluginCacheControl } from 'apollo-server-core';
import responseCachePlugin from 'apollo-server-plugin-response-cache';

import { Satellite } from '@/core/types';
import { getAbove, getVisualPasses } from '@/services/n2yo';


const typeDefs = gql`
  enum CacheControlScope {
    PUBLIC
    PRIVATE
  }

  directive @cacheControl(
    maxAge: Int
    scope: CacheControlScope
    inheritMaxAge: Boolean
  ) on FIELD_DEFINITION | OBJECT | INTERFACE | UNION

  type Satellite {
    satid: ID
    satname: String
    intDesignator: String
    launchDate: String
    satlat: Float
    satlng: Float
    satalt: Float
    passes: [Int]
  }

  # Each call we have:
  #  - 1 request for satellites above
  #  - 28 (maximum for galileo) calls if all galileos satellites are above
  #  so with a cache limite at 2 minutes should be fine ;)
  type Above @cacheControl(maxAge: 120, scope: PUBLIC) {
    category: String
    satellites: [Satellite]
  }

  type Query {
    above: Above
  }
`;

const resolvers = {
  Query: {
    above: async () => {
      const data = await getAbove();
      console.debug('----> n2yo api called');

      // sort by orbital velocity
      // For circular orbit, velocity seems to equal: √(GM/ satalt)
      // So if I remember well my maths lessons.
      // The biggest salt will be the smallest speed will be with this equation
      // So we sort satellites by alt order.
      const satellites = (data?.above || [])
        .sort(
          (a, b) => {
            if (a.satalt > b.satalt) return 1;
            if (a.satalt < b.satalt) return -1;
            return 0;
          }
        );

      const results = await Promise.all(
        satellites.map(
          getVisualPasses
        )
      );

      const positions: { [key: string]: number[] } = results.reduce(
        (o, { satid, passes }) => ({ ...o, [satid]: passes }),
        {}
      );

      return {
        category: data?.info?.category,
        satellites: satellites.map(
          (v) => ({ ...v, passes: positions?.[v.satid] })
        )
      };
    },
  },
};

const apolloServer = new ApolloServer({
  typeDefs,
  resolvers,
  plugins: [
    responseCachePlugin(),
    ApolloServerPluginCacheControl({
      defaultMaxAge: 120,
      //calculateHttpHeaders: true,
    })
  ],
});

const startServer = apolloServer.start();

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  await startServer;
  await apolloServer.createHandler({
    path: "/api/graphql",
  })(req, res);
}

export const config = {
  api: {
    bodyParser: false,
  }
};
