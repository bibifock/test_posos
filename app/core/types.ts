export type Satellite = {
  satid: string,
  satname: string,
  satlat: number,
  satlng: number,
  satalt: number,
  passes: number[]
};
