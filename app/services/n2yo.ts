import axios from 'axios';

import { Satellite } from '@/core/types';

const apiKey = process.env.API_KEY || '';

const categoryId = process.env.SATELLITE_CATEGORY;

const lat = process.env.OFFICE_LAT;
const lng = process.env.OFFICE_LNG;
const alt = process.env.OFFICE_ALT;
const radius = 90;

type Above = {
  info: {
    category: string;
  };
  above: {
    satid: string;
    satname: string;
    intDesignator: string;
    launchDate: string;
    satlat: number;
    satlng: number;
    satalt: number;
  }[]
}

type GetAbove = () => Promise<Above>;

export const getAbove: GetAbove = () => axios.get(
  `https://api.n2yo.com/rest/v1/satellite/above/${lat}/${lng}/${alt}/${radius}/${categoryId}?apiKey=${apiKey}`
).then(v => v.data);

type N2YOVisualPasses = {
  info: {
    satid: string;
  }
  passes: { startUTC: number; }[];
};

type GetVisualPasses = (args: { satid: string; }) => Promise<{ satid: string; passes: number[] }>;

export const getVisualPasses: GetVisualPasses = ({ satid }) => axios.get<N2YOVisualPasses>(
  `https://api.n2yo.com/rest/v1/satellite/visualpasses/${satid}/${lat}/${lng}/${alt}/2/300?apiKey=${apiKey}`
).then(({ data }) => ({
  satid,
  passes: (data?.passes || []).map(v => v.startUTC)
}));
