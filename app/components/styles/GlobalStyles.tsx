import React, { FC } from 'react';
import { createGlobalStyle } from 'styled-components';
import tw, { theme, GlobalStyles as BaseStyles } from 'twin.macro';

const GlobalStyles: FC = () => (
  <BaseStyles />
);

export default GlobalStyles;
