import React, { FC } from 'react';
import styled from 'styled-components';
import tw from 'twin.macro';

import Satellite, { SatelliteProps } from '@/components/satellite/Satellite';

const Item = styled(Satellite)`
  ${tw`hover:bg-blue-50`}
`;

const Header = styled.div`
  display: flex;
  flex-direction: row;
  align-items: stretch;

  ${tw`border-b border-gray-200 p-5`}

  h1 {
    flex-grow: 1;
    ${tw`text-blue-800 font-bold`}
  }
`;

const Button = styled.button`
  ${tw`py-1 px-3 border border-transparent text-base font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700`}
`;

const List = styled.div`
  ${tw`divide-y divide-gray-200 w-full`}
`;

const renderList = (item: SatelliteProps) => <Item {...item} key={item.satid} />;

export type ListAboveProps = {
  className?: string;
  category: string;
  satellites: SatelliteProps[];
  onRefresh: () => void;
};

const ListAbove:FC<ListAboveProps> = ({
  className,
  satellites,
  category,
  onRefresh
}) => (
  <div className={className}>
    <Header>
      <h1>{category}</h1>
      <div>
        <Button onClick={onRefresh}>refresh</Button>
      </div>
    </Header>

    <List>
      {satellites.map(renderList)}
    </List>
  </div>
);

export default ListAbove;
