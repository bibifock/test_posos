import React, { FC } from 'react';
import styled from 'styled-components';
import tw from 'twin.macro';

import { Satellite as SatelliteType } from '@/core/types';

const Ul = styled.ul`
  ${tw`list-disc ml-10`}
`;

const Values = styled.div`
  display: flex;
  gap: 5px;

  strong {
    ${tw`text-gray-500`}
  }
`;

const ColValues = styled.div`
  flex-direction: column;
`;

const Container = styled.div`
  display: flex;
  align-items: start;
  ${tw`px-5 py-2`}

  > div {
    display: flex;
    flex-direction: column;
    justify-content: space-between;

    &:first-child {
      flex-grow: 1;
    }
  }
`;

const SpanId = styled.span`
  ${tw`text-gray-500 font-bold`}
`;

const SpanName = styled.span`
  ${tw`text-gray-800 font-semibold`}
`;

type RenderPasses = (satid: string) => (i: number, k: number) => React.ReactElement;

// eslint-disable-next-line react/display-name
const renderPasses: RenderPasses = (satid) => (i) => (
  <span key={`${satid}__${i}`}>
    {(new Date(i * 1000)).toUTCString()}
  </span>
);

export type SatelliteProps = SatelliteType & {
  className?: string
};

const Satellite:FC<SatelliteProps> = ({
  satid,
  satname,
  satlat,
  satlng,
  satalt,
  passes,
  className
}) => (
  <Container className={className}>
    <ColValues>
      <Values>
        <SpanId>#{satid}</SpanId>
        <SpanName>{satname}</SpanName>
      </Values>
      <Ul>
        <li>lat: <strong>{satlat}</strong></li>
        <li>lng: <strong>{satlng}</strong></li>
        <li>alt: <strong>{satalt}</strong></li>
      </Ul>
    </ColValues>
    <Values>
      <strong>Next passes above</strong>
      {passes
        ? passes.map(renderPasses(satid))
        : null
      }
    </Values>
  </Container>
);

export default Satellite;
